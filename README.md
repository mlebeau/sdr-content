# SDR Content

Repo for SDR Content (snippets, use cases, personas, etc)

[WIP: Use Case / Persona Matrix](https://docs.google.com/spreadsheets/d/1cyZZhE0G182i16esy5HhfZLMy6mavhuZNydoZXOdVCI/edit#gid=0)
[Snippet Library](https://docs.google.com/spreadsheets/d/12Pf3uynZ-hrRwtMUodsj-LrkRMAIgST_mNWygTl2kAI/edit#gid=501703648)
